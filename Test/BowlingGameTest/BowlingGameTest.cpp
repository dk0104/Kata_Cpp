//
// Created by DK on 03/05/15.
//

#include "BowlingGameTest.h"

BowlingGameTest::BowlingGameTest() {

}

BowlingGameTest::~BowlingGameTest() {

}

void BowlingGameTest::TearDown() {

}

void BowlingGameTest::SetUp() {
    game = new Game();
}

void BowlingGameTest::RollMany(int rounds,int pins)
{
    for (int i = 0; i < rounds; ++i) {
        game->Roll(pins);
    }
}

TEST_F(BowlingGameTest, GutterGameTest)
{
    RollMany(20,0);
    EXPECT_EQ(0,game->Score());
}

TEST_F(BowlingGameTest,TestAllOnes)
{
    RollMany(20,1);
    EXPECT_EQ(20,game->Score());
}

TEST_F(BowlingGameTest,TestOneSpare)
{
    game->Roll(5);
    game->Roll(5);
    game->Roll(3);
    RollMany(17,0);
    EXPECT_EQ(16,game->Score());
}