//
// Created by DK on 03/05/15.
//

#ifndef CPPKATA_BOWLINGGAMETEST_H
#define CPPKATA_BOWLINGGAMETEST_H

#include "gtest/gtest.h"
#include "../../BowlingGame/Game.h"


class BowlingGameTest: public::testing::Test {
protected:
    BowlingGameTest();
    ~BowlingGameTest();

    virtual void SetUp();
    virtual void TearDown();

    void RollMany(int rounds,int pins);

    Game* game;
};

#endif //CPPKATA_BOWLINGGAMETEST_H
