//
// Created by DK on 20/06/15.
//

#include "Frame.h"

int Frame::Score() {
    int score=0;
    for (int i = 0; i <rollCount ; ++i) {
        score+=rolls[i].pins;
    }
    return score;
}

Frame::~Frame() {

}

Frame::Frame() {
    rolls=new Roll[rollCount];
    currentRoll=0;
}

bool Frame::SetNextRoll(int pin) {
    if(currentRoll < 2)
    {
        rolls[currentRoll].pins=pin;
        currentRoll++;
        return true;
    }
    return false;
}


int Frame::SetPins(int pin) {
    return SetNextRoll(pin);
}

bool Frame::CheckSpare() {
        return rolls[0].pins+rolls[1].pins==10;
}

bool Frame::CheckStrike() {
    return rolls[0].pins==10;
}

int Frame::GetFirstRoll() {
    return rolls[0].pins;
}
