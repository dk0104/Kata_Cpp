//
// Created by DK on 03/05/15.
//

#include "Game.h"

int Game::Score() {
    int score=0;
    for (int i = 0; i <FrameCount ; ++i) {
        score+=frames[i].Score();
        if(frames[i].CheckSpare())
        {
            score+=frames[i+1].GetFirstRoll();
        }
        if(frames[i].CheckStrike())
        {
            score+=frames[i+1].Score();
        }
    }
    return score;
}

void Game::Roll(int pin) {
    if(currentFrame<FrameCount)
    {
        if(!frames[currentFrame].SetPins(pin))
        {
            currentFrame++;
            frames[currentFrame].SetPins(pin);
        }
    }
}

Game::Game() {

    frames = new Frame[FrameCount];
    currentFrame=0;
}

Game::~Game() {
    delete frames;
}

int Game::SetNextRoll(int pin) {
    return 0;
}
