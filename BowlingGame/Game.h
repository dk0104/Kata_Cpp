//
// Created by DK on 03/05/15.
//

#ifndef CPPKATA_GAME_H
#define CPPKATA_GAME_H

#include "Frame.h"

class Game {
public:
    Game();
    ~Game();
    void Roll(int pin);
    int Score();

private:
    int SetNextRoll(int pin);
    Frame* frames;
    const int FrameCount=10;
    int currentFrame;

};

#endif //CPPKATA_GAME_H
