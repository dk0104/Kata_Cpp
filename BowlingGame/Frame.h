//
// Created by DK on 20/06/15.
//

#ifndef CPPKATA_FRAME_H
#define CPPKATA_FRAME_H

#include "Roll.h"

class Frame {
public:
    Frame();
    ~Frame();
    int SetPins(int pin);
    int Score();
    bool CheckSpare();
    bool CheckStrike();
    int GetFirstRoll();

private:
    bool SetNextRoll(int pin);
    Roll* rolls;
    const int rollCount=2;
    int currentRoll;

};

#endif //CPPKATA_FRAME_H
